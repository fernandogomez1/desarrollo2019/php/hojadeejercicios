<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        class Animal{
            var $name;
            function set_name($text){
                $this->name=$text;
            }
            function get_name(){
                return $this->name;
            }
        }
        
        class leon extends Animal{
            var $name;
            function roar(){
                echo "=>", $this->name, " esta rugiendo!<br>";
            }
            function set_name($text) {
                //Animal::set_name($text);
                $this->name=$text;
            }
        }
        
        echo "creando un nuevo leon..<br>";
        $leon=new leon;
        $leon->set_name("superman");
        $leon->roar();
        ?>
    </body>
</html>
