<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!-- todo lo que tiene name se envía a FicheroDelFormulario.php --> 
        <form action="FicheroDelFormulario.php" method="get">
            <label for="inombre">Nombre del alumno</label>
            <input type="text" id="inombre" name="nombre">
            <label for="inumero1">Numero 1</label>
            <!-- para no poner nombre a cada uno, se puede hacer así-->
            <input type="text" id="inumero1" name="inumero1">
            <label for="inumero2">Numero 2</label>
            <input type="text" id="inumero2" name="inumero2">
            <!-- para no poner nombre a cada uno, se puede hacer así-->
            <label for="numero3">Numero 3</label>
            <input type="text" id="numero3" name="numero[]">
             <label for="numero4">Numero 4</label>
            <input type="text" id="numero4" name="numero[]">
            <div>
                <label for="ipotes">Potes</label>
                <input type="checkbox" name="poblacion[]" value="Potes" id="ipotes" />
                <label for="isantander">Santander</label>
                <input type="checkbox" name="poblacion[]" value="Santander" id="isantander"/>
            </div>
            <div>
                <label for="">Rojo</label>
                <input type="radio" name="colores" value="Rojo" />
                <label for="">Azul</label>
                <input type="radio" name="colores" value="Azul" />
            </div>
            <div>
                <!--si se usa multiple se comporta como un cuadro de lista y hay que poner corchetes, se sele4ccionan varios con 
                la tecla control-->
                <label for="">Selecciona nombres</label>
                <select name="nombres[]" id="" multiple="true">
                    <option value="0">Roberto</option>
                    <option value="1">Silvia</option>
                    <option value="2">Laura</option>
                </select>
            </div>
            <!--<input type="submit" value="Enviar" />-->
            <button>Enviar</button>
        </form>
        <?php
        
        ?>
    </body>
</html>
