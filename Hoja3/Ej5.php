<?php
$Array1=array(1,3,5,2,3,2,7,7,7,7,7,7,7,7,7,7,7,7,7);
$Array2=array();


/**
 * Funcion que devuelve el numero que se repite y cuantas veces
 * @param type $array Esteo es un array con numeros.
 * @param type $devolverTodos Esto es para que devuelva todos los numeros o solo los repetidos
 * @return array
 */
function elementosRepetidos($array, $devolverTodos=false){
    $repeated=array();
    
    foreach((array) $array as $value){
        $inArray=false;
        foreach ($repeated as $i =>$rItem){
            if($rItem['value']===$value){
                $inArray=true;
                ++$repeated[$i]['count'];
            }
        }
    
    
        if (false===$inArray){
            $i=count($repeated);
            $repeated[$i]=array();
            $repeated[$i]['value']=$value;
            $repeated[$i]['count']=1;
        }
    }
    
    if(!$devolverTodos){
        foreach ($repeated as $i => $rItem){
            if($rItem['count']===1){
                unset($repeated[$i]);
            }
        }
    }
    sort($repeated);
    return $repeated;
}
$Array2= elementosRepetidos($Array1, true);
echo "array1";
var_dump($Array1);
echo "array2";
var_dump($Array2);
