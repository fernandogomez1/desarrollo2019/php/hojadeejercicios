<?php

$a=array();
/**
 * Esta funcion genera una serie de numeros aleatorios
 * @param int $vMinimo Este es el valor minimo
 * @param int $vMaximo este es el valor maximo
 * @param int $nValores Numero de valores a generar
 * @return int[] el conjunto  de numeros solicitado
 */

function generaVectorNaleatorios($vMinimo, $vMaximo, $nValores){
    $vector=[];
    for($i=0; $i<$nValores; $i++){
        $vector[$i]= random_int($vMinimo, $vMaximo);        
    }
    return $vector;
}
$a=generaVectorNaleatorios(5, 12, 7);
var_dump($a);


