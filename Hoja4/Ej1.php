<?php

class Usuario{
    private $nombre="Defecto";
    function setUsuario(){
        $this->nombre="Ramon";
    }
    function getNombre(){
        return $this->nombre;
    }
}

$persona=new Usuario();
echo $persona->getNombre()."<br>";
var_dump($persona);
$persona->setUsuario();
echo $persona->getNombre();
//esto da error al ser privada la variable
//echo $persona->nombre;
$persona2=new Usuario("33");
var_dump($persona2);