<?php

class Vehiculo{
    public $matricula;
    private $color;
    protected $encendido;
    private static $mensaje="mensaje estatico";
    
    function __construct($matricula, $color, $encendido){
        $this->matricula=$matricula;
        $this->color=$color;
        $this->encendido=$encendido;
    }
    function __construct($matricula){
        $this->matricula=$matricula;
        $this->color="rojo";
        $this->encendido=false;
    }
    
    public function encender() {
        $this->encendido=true;
        echo 'Vehiculo encendido <br/>';
    }
    
    public function apagar() {
        $this->encendido=false;
        echo 'vehiculo apagado';
    }
    public function muestraUnMensaje(){
        //$this->mensaje="Hola desde muestra mensaje";
        echo "<br>";
        echo Vehiculo::$mensaje;
    }
    
}
